<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <title>Login Page</title>
        <style type="text/css">
            .btn{
                width:20% !important;
                padding:12px 16px 12px 16px !important;
                font-size:16px;
            }
            .error{
                color: #FF0000;
            }
        </style>
    </head>
    <?php
        session_start();
        if(isset($_SESSION["usr"])){
            header('Location:welcome.php');
        }
        if(isset($_COOKIE["usr"]) && isset($_COOKIE["secure"]))
        {
            $_SESSION["usr"] = $_COOKIE["usr"];
            $_SESSION["secure"] = $_COOKIE["secure"];
            header('Location:welcome.php');
        }
        else
        {
            if(isset($_GET["x"]))
            {
                if($_GET["x"]==1){
                    echo '<script>alert("Please Refer To your mail for the login info")</script>';
                }
                if($_GET["x"]==2){
                    echo '<script>alert("You Have Been Successfully Logged Out")</script>';
                }
                if($_GET["x"] == 3){
                    echo '<script>alert("You Are Not Atuthorized To Access This Page. Please Login To Continue")</script>';
                }
                if($_GET["x"] == 4){
                    echo '<script>alert("You Have Successfully Reset Your Password. Please Login To Continue")</script>';
                }
            }
            if(isset($_POST["login"]))
            {
                $servername = "localhost";
                $username = "root";
                $password = "mindfire";
                $dbname = "user_master";

                try{
                    // Create connection
                    $conn = new mysqli($servername, $username, $password,$dbname);
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 

                    $user = $_POST["usr"];
                    $pass = $_POST["pass"];
                    $count = 2;
                    if (empty($_POST["usr"])) 
                    {
                        $emailErr = "User Name is required";
                    } 
                    else 
                    {
                        $email = test_input($_POST["usr"]);
                        // check if e-mail address is well-formed
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                        {
                            $emailErr = "Invalid Username Format"; 
                        }
                        else{
                            $count--;
                        }
                    }

                    if (empty($_POST["pass"])) 
                    {
                        $passErr = "Password is required";
                    } 
                    else 
                    {
                        $pass = $_POST["pass"];
                        // check if password satisfies all conditions
                        if (!preg_match("((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})",$pass)) 
                        {
                            $passErr = "Invalid Password Format"; 
                        }
                        else{
                            $count--;
                        }
                    }
                    
                    if($count == 0)
                    {
                        $pass = md5($pass);
                        $sql = "select * from user_details where email = '$user' AND pass = '$pass'";

                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) 
                        {
                            // output data of each row
                            $row = $result->fetch_assoc();
                            $_SESSION["usr"] = $row["fname"];
                            $_SESSION["secure"] = 1;

                            if(isset($_POST["rem"]))
                            {
                                $cookie_name = "usr";
                                $cookie_value = $row["fname"];
                                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/');
                                $cookie_name2 = "secure";
                                $cookie_value2 = 1;
                                setcookie($cookie_name2, $cookie_value2, time() + (86400 * 30), '/');
                            }
                            header('Location:welcome.php');
                        }
                        else{
                            echo '<script>alert("Wrong Username Or Password")</script>';
                        }
                    }
                }
                catch (Exception $e) {
                    echo '<script>alert("Error while connecting to database!")</script>';
                    die;
                    echo '<script>window.location(error.php)</script>';
                }
            }  
        }

        function test_input($data) 
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        } 
    ?>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h4 align="center">Please Login To Continue</h4><br>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                        <div class="form-group">
                            <label for="email">Login ID</label>
                            <input type="text" class="form-control" id="fname" name="usr" >
                            <label for="email" class="error" id="lnameErr"><?php echo $emailErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="email">Password</label>
                            <input type="password" class="form-control" id="mname" name="pass">
                            <label for="email" class="error" id="lnameErr"><?php echo $passErr; ?></label>
                        </div>
                        <div class="checkbox">
                            <label><input class="" type="checkbox" name="rem" id="hobbies"><span class="pins">Remember Me</label>
                        </div>
                        <div class="form-group">
                            <label for="email"></label>
                        </div>
                        <div class="form-group">
                            <label for="email"><a href="forgot.php">Forgot Password</a></label>
                        </div>
                        <div class="form-group">
                            <label for="email">New User?<a href="register.php">Register Here</a></label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-4">
                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary" name="login">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer">
        </div>
    </body>
</html>