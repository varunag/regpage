$("document").ready(function() {

    $("#fname").blur(function(){

        if( $(this).val().length === 0 ){
          $("#fnameErr").text("This Field Cannot be left empty") ; 
        }
        
        else {

            var VAL = this.value;
            var fname = new RegExp('^[A-Za-z ]*$');
            if (!fname.test(VAL)) {
                $("#fnameErr").text("Only letters and white space allowed");
            }
            else {
                $("#fnameErr").text("");
            }
        }
    });

    $("#mname").blur(function(){

        if( $(this).val().length === 0 ){
          $("#mnameErr").text(" ") ; 
        }
        var VAL = this.value;

        var mname = new RegExp('^[A-Za-z ]*$');

        if (!mname.test(VAL)) {
           $("#mnameErr").text("Only letters and white space allowed");
        }
        else{
            $("#mnameErr").text("");
        }
    });

    $("#lname").blur(function(){

        if( $(this).val().length === 0 ){
          $("#lnameErr").text("This Field Cannot be left empty") ; 
        }
        var VAL = this.value;

        var lname = new RegExp('^[A-Za-z ]*$');

        if (!lname.test(VAL)) {
           $("#lnameErr").text("Only letters and white space allowed");
        }
        else {
           $("#lnameErr").text("");
        }
    });

    $("#email").blur(function(){

        if( $(this).val().length === 0 ){
          $("#emailErr").text("This Field Cannot be left empty") ; 
        }
        var VAL = this.value;

        var email = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!email.test(VAL)) {
           $("#emailErr").text("Not Valid Email Format");
        }
        else {
           $("#emailErr").text("");
        }
    });

    $("#phno").blur(function(){

        if( $(this).val().length === 0 ){
          $("#phErr").text("This Field Cannot be left empty") ; 
        }
        var VAL = this.value;

        var phno = new RegExp('^[0-9]{10}$');

        if (!phno.test(VAL)) {
           $("#phErr").text("Phone No Should Be Exactly Of 10 Digits");
        }
        else {
           $("#phErr").text("");
        }
    });

    $("#pwd").blur(function(){

        if( $(this).val().length === 0 ){
          $("#passErr").text("This Field Cannot be left empty") ; 
        }
        var VAL = this.value;

        var pass = new RegExp('^[0-9]{10}$');

        if (!pass.test(VAL)) {
           $("#passErr").text("Invalid Password");
        }
        else {
           $("#passErr").text("");
        }
    });

    $("#pwd").blur(function(){

        if( $(this).val().length === 0 ){
          $("#passErr").text("This Field Cannot be left empty") ; 
        }
        var VAL = this.value;

        var pass = new RegExp('^[0-9]{10}$');

        if (!pass.test(VAL)) {
           $("#passErr").text("Invalid Password");
        }
        else {
           $("#passErr").text("");
        }
    });

    $("#repwd").blur(function(){

        if( $(this).val().length === 0 ){
          $("#passErr").text("This Field Cannot be left empty") ; 
        }
        

        if ($(this).val === $("#pwd").val) {
           $("#repassErr").text("Re Entered Password Does Not Match The Password");
        }
        else {
           $("#passErr").text("");
        }
    });



});