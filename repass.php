<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <<title>Re Create Password</title>
        <style type="text/css">
            .btn{
                width:20% !important;
                padding:12px 16px 12px 16px !important;
                font-size:16px;
            }
        </style>
    </head>
    <?php
        session_start();
        if(isset($_POST["submit"])){
            $count = 2;
            $passErr = $repassErr = "";

            if (empty($_POST["pwd"])) 
            {
                $passErr = "Password is required";
            } 
            else 
            {
                $pass = $_POST["pwd"];
                // check if password satisfies all conditions
                if (!preg_match("((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})",$pass)) 
                {
                    $passErr = "Invalid Password"; 
                }
                else
                {
                    $count--;
                }
            }

            //Retype Password Validation
            if (empty($_POST["repwd"])) 
            {
                $repassErr = "Re Type Password is required";
            } 
            else 
            {
                $repass = $_POST["repwd"];
                // chec if retyped password matches the previous password or not
                if ($repass != $pass)
                {
                    $repassErr = "Password Not Matching The Previous Password"; 
                }
                else
                {
                    $count--;
                }
            }

            if($count == 0){
                $servername = "localhost";
                $username = "root";
                $password = "mindfire";
                $dbname = "user_master";

                try{
                    // Create connection
                    $conn = new mysqli($servername, $username, $password,$dbname);
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 
                    $email = $_SESSION["email"];
                    $pass = md5($_POST["pwd"]);
                    $sql = "UPDATE user_details SET pass = '$pass' WHERE email = '$email'";

                    if ($conn->query($sql) === TRUE) 
                    {
                        // output data of each row
                        $conn->commit();
                        header('Location:login.php?x=4');
                    }
                    else{
                        echo '<script>alert("Wrong Username Or Password")</script>';
                        throw new Exception('Error');
                    }
                }
                catch( Exception $e)
                {
                    $conn->rollback();
                }
            }

        }
    ?>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h4 align="center">Please Enter New Password</h4><br>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                        <div class="form-group">
                            <label for="email">New Password</label>
                            <input type="password" class="form-control" id="fname" name="pwd" >
                            <label for="email" ><?php echo $passErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="email">ReType New Password</label>
                            <input type="password" class="form-control" id="mname" name="repwd">
                            <label for="email"><?php echo $repassErr; ?></label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-4">
                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer">
        </div>
    </body>
</html>  
    