<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <title>Registration Form</title>
        <style>
            body{
                background: linear-gradient(to bottom right,#FF7738,#FFC06F,#FFEDAF,#FFFFFF);
            }
            h1{
                text-align: center;
            }
            .footer{
                height: 100px;
            }
            @media (max-width: 991px) {
                    .form-group {
                    padding-left:0;
                    padding-right: 0;
                }
            }
            .remove-left{
                padding-left: 0 !important;
                margin-left: 0 !important;
            }
            .remove-right{
                padding-right: 0 !important;
                margin-right: 0 !important;
            }
            .error{
                color: #FF0000;
            }
            .btn{
                width:20% !important;
                padding:12px 16px 12px 16px !important;
                font-size:16px;
            }
        </style>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip(); 
            });
        </script>
    </head>
    <body>


        <?php
            // define variables and set to empty values
            $fnameErr = $mnameErr = $lnameErr = $emailErr = $phnoErr = $passErr = "";
            $repassErr = $genderErr = $dobErr = "";
            $fname = $mname = $lname = $email = $gender = $phno = $pass = $repass = $dob = "";
            $count = 19;

            if ($_SERVER["REQUEST_METHOD"] == "POST") 
            {
                //First Name Validation
                if (empty($_POST["fname"])) 
                {
                    $fnameErr = "First Name is required";
                } 
                else 
                {
                    $fname = test_input($_POST["fname"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$fname)) 
                    {
                        $fnameErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                //Middle Name Validation
                if (empty($_POST["mname"])) 
                {
                    $mnameErr = "";
                } 
                else 
                {
                    $mname = test_input($_POST["mname"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$mname)) 
                    {
                        $mnameErr = "Only letters and white space allowed"; 
                    }
                }

                //Last Name Validation
                if (empty($_POST["lname"])) 
                {
                    $lnameErr = "First Name is required";
                } 
                else 
                {
                    $lname = test_input($_POST["lname"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$lname)) 
                    {
                        $lnameErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }
                
                //Email Validation
                if (empty($_POST["email"])) 
                {
                    $emailErr = "Email is required";
                } 
                else 
                {
                    $email = test_input($_POST["email"]);
                    // check if e-mail address is well-formed
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                    {
                        $emailErr = "Invalid Email Format"; 
                    }
                    else{
                        $y = checkExisting($email);
                        if($y == 1)
                        {
                            $count--;
                        }
                        else{
                            $emailErr = "Email Already Exist. Please Select A New One.";
                        }
                    }
                }
                
                    
                //Phone No Validation
                if (empty($_POST["phno"])) 
                {
                    $phnoErr = "Phone No Should Not Be Empty";
                } 
                else 
                {
                    $phno = test_input($_POST["phno"]);
                    // check if the phone no has exactly 10 digits or not
                    if (!preg_match("/^[0-9]{10}$/",$phno)) 
                    {
                        $phnoErr = "Invalid Phone No. It Should Be 10 Digit No. "; 
                    }
                    else{
                        $count--;
                    }
                }

                //Password Validation
                if (empty($_POST["pwd"])) 
                {
                    $passErr = "Password is required";
                } 
                else 
                {
                    $pass = $_POST["pwd"];
                    // check if password satisfies all conditions
                    if (!preg_match("((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})",$pass)) 
                    {
                        $passErr = "Invalid Password"; 
                    }
                    else
                    {
                        $count--;
                    }
                }

                //Retype Password Validation
                if (empty($_POST["repwd"])) 
                {
                    $repassErr = "Re Type Password is required";
                } 
                else 
                {
                    $repass = $_POST["repwd"];
                    // chec if retyped password matches the previous password or not
                    if ($repass != $pass)
                    {
                        $repassErr = "Password Not Matching The Previous Password"; 
                    }
                    else
                    {
                        $count--;
                    }
                }

                //Gender Validation
                if (empty($_POST["gender"])) 
                {
                    $genderErr = "Gender is required";
                } 
                else 
                {
                    $gender = test_input($_POST["gender"]);
                    $count--;
                }
                
                //Date Of Birth Validation
                if(empty($_POST["dob"]))
                {
                    $dobErr = "Choose A date";
                }
                else
                {
                    $dob = test_input($_POST["dob"]);
                    if (!preg_match("/^[0-9- ]{10}$/",$dob)) 
                    {
                        $dobErr = "Invalid Date";
                    }
                    else{
                        $count--;
                    }
                }


                $padhno = $padcit = $padsta = $padcon = $padzip = "";
                $cadhno = $cadcit = $cadsta = $cadcon = $cadzip = "";
                $padhnoErr = $padcitErr = $padstaErr = $padconErr = $padzipErr = "";
                $cadhnoErr = $cadcitErr = $cadstaErr = $cadconErr = $cadzipErr = "";


                // Permanent Address Validation


                // House No

                if (empty($_POST["padd_hno"])) 
                {
                    $padhnoErr = "House No  is required";
                } 
                else 
                {
                    $padhno = test_input($_POST["padd_hno"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[0-9a-zA-Z- ]*$/",$padhno)) 
                    {
                        $padhnoErr = "Only letters, numbers and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // City

                if (empty($_POST["padd_city"])) 
                {
                    $padcitErr = "City is required";
                } 
                else 
                {
                    $padcit = test_input($_POST["padd_city"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$padcit)) 
                    {
                        $padcitErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // State

                if (empty($_POST["padd_state"])) 
                {
                    $padstaErr = "State is required";
                } 
                else 
                {
                    $padsta = test_input($_POST["padd_state"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$padsta)) 
                    {
                        $padstaErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // Country

                if (empty($_POST["padd_con"])) 
                {
                    $padconErr = "Country is required";
                } 
                else 
                {
                    $padcon = test_input($_POST["padd_con"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$padcon)) 
                    {
                        $padconErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // Zip Code
                if (empty($_POST["padd_zip"])) 
                {
                    $padzipErr = "Zip Code is required";
                } 
                else 
                {
                    $padzip = test_input($_POST["padd_zip"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[0-9 ]*$/",$padzip)) 
                    {
                        $padzipErr = "Only Numbers allowed"; 
                    }
                    else{
                        $count--;
                    }
                }



                // Current Address Validation


                // House No

                if (empty($_POST["cadd_hno"])) 
                {
                    $cadhnoErr = "House No  is required";
                } 
                else 
                {
                    $cadhno = test_input($_POST["cadd_hno"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[0-9a-zA-Z- ]*$/",$cadhno)) 
                    {
                        $cadhnoErr = "Only letters, numbers and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // City

                if (empty($_POST["cadd_city"])) 
                {
                    $cadcitErr = "City is required";
                } 
                else 
                {
                    $cadcit = test_input($_POST["cadd_city"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$cadcit)) 
                    {
                        $cadcitErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // State

                if (empty($_POST["cadd_state"])) 
                {
                    $cadstaErr = "State is required";
                } 
                else 
                {
                    $cadsta = test_input($_POST["cadd_state"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$cadsta)) 
                    {
                        $cadstaErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // Country

                if (empty($_POST["cadd_con"])) 
                {
                    $cadconErr = "Country is required";
                } 
                else 
                {
                    $cadcon = test_input($_POST["cadd_con"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z ]*$/",$cadcon)) 
                    {
                        $cadconErr = "Only letters and white space allowed"; 
                    }
                    else{
                        $count--;
                    }
                }

                // Zip Code
                if (empty($_POST["cadd_zip"])) 
                {
                    $cadzipErr = "Zip Code is required";
                } 
                else 
                {
                    $cadzip = test_input($_POST["cadd_zip"]);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[0-9 ]*$/",$cadzip)) 
                    {
                        $cadzipErr = "Only Numbers allowed"; 
                    }
                    else{
                        $count--;
                    }
                }


                // Personal Interest Validation
                
                if(!isset($_POST['hobbies']))
                {
                    $hobErr = "Please Choose One Hobby"; 
                }
                else 
                {   
                    $hobbies = implode(",",$_POST["hobbies"]);  
                    $hobbies = test_input($hobbies);
                    // check if name only contains letters and whitespace
                    if (!preg_match("/^[a-zA-Z_,]*$/",$hobbies)) 
                    {
                        $hobErr = "You can't Do That";
                        echo '<script>alert("That is hacking bro....Got you")</script>';
                    }
                    else{
                        $count--;
                    }
                }

                

                if($count == 0)
                {
                    connectdb();
                }

            }
            function checkExisting($em)
            {
                $servername = "localhost";
                $username = "root";
                $password = "mindfire";
                $dbname = "user_master";

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT * from user_details where email = '$em'";

                $result = $conn->query($sql);
                if ($result->num_rows > 0) 
                {
                    $c = -1;
                }
                else{
                    $c = 1;
                }

                $conn->close();
                return $c;
            }
                
            function connectdb()
            {
                $servername = "localhost";
                $username = "root";
                $password = "mindfire";
                $dbname = "user_master";

                // Create connection
                try
                {
                    $conn = new mysqli($servername, $username, $password,$dbname);
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 
                    

                    $fname = $_POST["fname"];
                    $mname = $_POST["mname"];
                    $lname = $_POST["lname"];
                    $email = $_POST["email"];
                    $phno = $_POST["phno"];
                    $pass = md5($_POST["pwd"]);
                    $gender = $_POST["gender"];
                    $dob = $_POST["dob"];
                    $padhno = $_POST["padd_hno"];
                    $padcit = $_POST["padd_city"];
                    $padsta = $_POST["padd_state"];
                    $padcon = $_POST["padd_con"];
                    $padzip = $_POST["padd_zip"];
                    $cadhno = $_POST["cadd_hno"];
                    $cadcit = $_POST["cadd_city"];
                    $cadsta = $_POST["cadd_state"];
                    $cadcon = $_POST["cadd_con"];
                    $cadzip = $_POST["cadd_zip"];
                    $hobbies = implode(",",$_POST["hobbies"]);  


                    // Instert Into Table
                    $sql = "INSERT INTO user_details (fname, mname, lname, email, phno , pass, gender, dob, hobbies) VALUES ('$fname', '$mname', '$lname', '$email', $phno , '$pass', '$gender', '$dob', '$hobbies')";
                    $sql2 = "INSERT INTO user_address(email, type, hno, city, state, country, zip) VALUES ('$email', 'p' , '$padhno', '$padcit', '$padsta', '$padcon', '$padzip')";
                    $sql3 = "INSERT INTO user_address(email, type, hno, city, state, country, zip) VALUES ('$email', 'c' , '$cadhno', '$cadcit', '$cadsta', '$cadcon', '$cadzip')";
                    if ($conn->query($sql) === TRUE )
                    {
                        if($conn->query($sql2) === TRUE && $conn->query($sql3) === TRUE) 
                        {
                            echo '<script>alert("Profile Created Successfully");</script>';
                            sendMail($email);
                            $conn->commit();
                        }
                        else{
                            throw new Exception('error!');
                        }
                    } else {
                        echo "Error: " . $sql . "<br>" . $conn->error;
                        throw new Exception('error!');
                    }
                }
                catch (Exception $e) 
                {
                    $conn->rollback();
                    echo '<script>alert("Error while connecting to database!")</script>';
                    die;
                    echo '<script>window.location(error.php)</script>';
                }
            }



            function sendMail($email)
            {
                require 'PHPMailer-master/PHPMailerAutoload.php';
                $to_name = "Varun";
                $to = $email;
                $subject = "Welcome To The Future";
                $message = "I Am very Happy That You Decided To Join With Us.<br>Your email will act as your login id.";
                $message = wordwrap($message,70);
                
                $from_name = "Zoro";
                $from = "offcld1@gmail.com";
                // phpMailer
                $mail = new PHPMailer();
                
                $mail->IsSMTP();
                $mail->Host = "smtp.gmail.com"; //enable php socks to make SSL it working
                $mail->Port = 465;
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = 'ssl';
                $mail->Username = "offcld1@gmail.com";
                $mail->Password = "qwezxcasd";
                
                $mail->FromName = $from_name;
                $mail->From = $from;
                $mail->AddAddress($to, $to_name);
                $mail->Subject = $subject;
                $mail->Body = $message;
                $mail->IsHTML(true);    
                $result = $mail->Send();
                echo $result ? header('Location:login.php?x=1') : 'Error'; // this line is optional - comment out as your need
            }



            
            function test_input($data) 
            {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>



        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h1>Registration Form</h1><br>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="form-group">
                        <label for="email">Already A Member? <a href="login.php">Login</a> to continue.</label><br>
                        <label for="email"><span class="error">* required fields</span></label>
                    </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12 remove-left">
                            <label for="email">First Name<span class="error">*</span></label>
                            <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $fname ?>">
                            <label for="email" class="error" id="fnameErr"><?php echo $fnameErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 ">
                            <label for="email">Middle Name</label>
                            <input type="text" class="form-control" id="mname" name="mname" value="<?php echo $mname ?>">
                            <label for="email" class="error" id="mnameErr"><?php echo $mnameErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 remove-right">
                            <label for="email">Last Name<span class="error">*</span></label>
                            <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $lname ?>">
                            <label for="email" class="error" id="lnameErr"><?php echo $lnameErr; ?></label>
                        </div>
                        <br>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="email">Email address<span class="error">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $email ?>">
                            <label for="email" class="error" id="emailErr"><?php echo $emailErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="email">Phone No<span class="error">*</span></label>
                            <input type="number" class="form-control" id="phno" name="phno" value="<?php echo $phno ?>">
                            <label for="email" class="error" id="phErr"><?php echo $phnoErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password<span class="error">*</span></label>
                            <input type="password" class="form-control" id="pwd" name="pwd" data-toggle="tooltip" data-placement="top" title="Password Should Contain Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character">
                            <label for="email" class="error" id="passErr"><?php echo $passErr; ?></label>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Retype Password<span class="error">*</span></label>
                            <input type="password" class="form-control" id="repwd" name="repwd" data-toggle="tooltip" data-placement="top" title="It Should be same as the above typed password">
                            <label for="email" class="error" id="repassErr"><?php echo $repassErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 remove-left">
                            <label for="email">Gender<span class="error">*</span></label><br>
                            <label class="radio-inline"><input type="radio" name="gender" value="male" <?php if (isset($gender) && $gender=="male") echo "checked";?>>Male</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="female" <?php if (isset($gender) && $gender=="female") echo "checked";?>>Female</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="others" <?php if (isset($gender) && $gender=="others") echo "checked";?>>Others</label><br>
                            <label for="email" class="error"><?php echo $genderErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 remove-right">
                            <label for="email">DOB<span class="error">*</span></label><br>
                            <input type="date" class="form-control" id="dob" name="dob" value="<?php echo $dob ?>">
                            <label for="email" class="error"><?php echo $dobErr; ?></label>
                        </div>

                        <div class="form-group">
                            <label></label>
                        </div>

                        <div class="form-group">
                            <label for="email">Permanent Address<span class="error">*</span></label><hr>
                        </div>   
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">House No(Separate with '-')</label>
                            <input type="text" class="form-control" id="padd_hno" name="padd_hno" value="<?php echo $padhno; ?>">
                            <label for="email" class="error"><?php echo $padhnoErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">City</label>
                            <input type="text" class="form-control" id="padd_city" name="padd_city" value="<?php echo $padcit; ?>">
                            <label for="email" class="error"><?php echo $padcitErr; ?></label>
                        </div>
                        <div class="clearfix" visible="col-lg col-md">
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">State</label>
                            <input type="text" class="form-control" id="padd_state" name="padd_state" value="<?php echo $padsta; ?>">
                            <label for="email" class="error"><?php echo $padstaErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Country</label>
                            <input type="text" class="form-control" id="padd_con" name="padd_con" value="<?php echo $padcon; ?>">
                            <label for="email" class="error"><?php echo $padconErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Zip Code</label>
                            <input type="text" class="form-control" id="padd_zip" name="padd_zip" value="<?php echo $padzip; ?>">
                            <label for="email" class="error"><?php echo $padzipErr; ?></label>
                        </div>

                        <div class="form-group">
                            <label></label>
                        </div>
                        <div class="form-group">
                            <label></label>
                        </div>
                        
                        <div class="form-group">
                            <label for="email">Current Address<span class="error">*</span></label><hr>
                        </div>   
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">House No(Separate with '-')</label>
                            <input type="text" class="form-control" id="cadd_hno" name="cadd_hno" value="<?php echo $cadhno; ?>">
                            <label for="email" class="error"><?php echo $cadhnoErr; ?></label>
                        </div>
                        <div class="form-group col-lg-6 col-md-6">
                            <label for="email">City</label>
                            <input type="text" class="form-control" id="cadd_city" name="cadd_city" value="<?php echo $cadcit; ?>">
                            <label for="email" class="error"><?php echo $cadcitErr; ?></label>
                        </div>
                        <div class="clearfix" visible="col-lg col-md">
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">State</label>
                            <input type="text" class="form-control" id="cadd_state" name="cadd_state" value="<?php echo $cadsta; ?>">
                            <label for="email" class="error"><?php echo $cadstaErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Country</label>
                            <input type="text" class="form-control" id="cadd_con" name="cadd_con" value="<?php echo $cadcon; ?>">
                            <label for="email" class="error"><?php echo $cadconErr; ?></label>
                        </div>
                        <div class="form-group col-lg-4 col-md-4">
                            <label for="email">Zip Code</label>
                            <input type="text" class="form-control" id="cadd_zip" name="cadd_zip" value="<?php echo $cadzip; ?>">
                            <label for="email" class="error"><?php echo $cadzipErr; ?></label>
                        </div>
                    
                        <div class="form-group">
                            <label>Personal Interests<span class="error">*</span></label><br>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies[]" value="sports" <?php if(isset($_POST['txtCheck'])) echo "checked='checked'"; ?>><span class="pins"> : Sports</span></label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies[]" value="books" <?php if(isset($_POST['txtCheck'])) echo "checked='checked'"; ?>><span class="pins"> : Books</span></label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies[]" value="comp_soft" <?php if(isset($_POST['txtCheck'])) echo "checked='checked'"; ?>><span class="pins"> : Computer And Software</span></label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies[]" value="fashion" <?php if(isset($_POST['txtCheck'])) echo "checked='checked'"; ?>><span class="pins"> : Fashion</span></label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies[]" value="photography" <?php if(isset($_POST['txtCheck'])) echo "checked='checked'"; ?>><span class="pins"> : Photograhy</span></label>
                            </div>
                            <div class="checkbox">
                                <label><input class="" type="checkbox" name="hobbies[]" value="cooking" <?php if(isset($_POST['txtCheck'])) echo "checked='checked'"; ?>><span class="pins"> : Cooking</span></label>
                            </div>
                            <div class="checkbox">
                                <label for="email" class="error" style="padding-left:0;"><?php echo $hobErr; ?></label>
                            </div>
                        </div>
                        
                        <div align = "center">
                            <button type="submit" class="btn btn-primary" name="submit">Register</button>
                        </div>
                    </form>
                
                </div>
            </div>
        </div>
        <div class="footer">
        </div>   
    </body>
</html>
